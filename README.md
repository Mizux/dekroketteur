# Dékroketteur

Ce projet est un démineur basé en lua.

## Comment lancer le projet

`love --console . 15 25 40`

- le premier nombre (15) correspond au nombre de colonnes
- le second au nombre de lignes 
- le dernier au nombre de mines

projet source : see Kromette 

Le but de ce projet est de proposer un démineur interactif avc le tchat Twitch à l'aide de :
 - NodeRed pour la liaison Twitch et le démineur
 - Löve2D pour l'interface et les logiques du démineur 
 
 le démineur est jouable en live en utilisant des points de chaine pour découvrir des cases ou placer des drapeaux indiquant l'emplacement des mines. 
 
 Ce projet étant créé avec la communauté, il sera évolutif.
